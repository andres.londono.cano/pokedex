
import Foundation

/// Home presenter declaration
protocol HomePresenterProtocol: BasePresenterProtocol {
    
    /// Sends a pokemon data request
    ///
    func getPokemonList()
    
    /// Sends a pokemon detail request
    ///
    func getPokemon(url: String, completion: ((_ pokemon: Pokemon) -> Void)?)
    
    /// Sends a moves data request
    ///
    func getMoveList()
    
    /// Sends a move detail request
    ///
    func getMove(url: String, completion: ((_ move: Move) -> Void)?)
    
    /// Sends a item data request
    ///
    func getItemList()
    
    /// Sends a move detail request
    ///
    func getItem(url: String, completion: ((_ item: Item) -> Void)?)
}
