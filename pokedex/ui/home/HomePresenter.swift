
import Foundation

/// Home presenter protocol implementation
///
/// Generic V paramenter must be a HomeView implementation
class HomePresenter<V: HomeView>: BasePresenter<V>, HomePresenterProtocol, PokemonServiceDelegate {
    
    private let pokemonService: PokemonService = PokemonService()
    
    func getPokemonList() {
        self.view?.showProgress(nil)
        self.pokemonService.getPokemonList(delegate: self)
    }
    
    func getPokemon(url: String, completion: ((_ pokemon: Pokemon) -> Void)?) {
        self.pokemonService.getPokemon(url: url, onSuccesfulGetPokemon: completion, onFailedGetPokemon: nil)
    }
    
    func getMoveList() {
        self.view?.showProgress(nil)
        self.pokemonService.getMoveList(delegate: self)
    }
    
    func getMove(url: String, completion: ((_ move: Move) -> Void)?) {
        self.pokemonService.getMove(url: url, onSuccesfulGetMove: completion, onFailedGetMove: nil)
    }
    
    func getItemList() {
        self.view?.showProgress(nil)
        self.pokemonService.getItemList(delegate: self)
    }
    
    func getItem(url: String, completion: ((_ item: Item) -> Void)?) {
        self.pokemonService.getItem(url: url, onSuccesfulGetItem: completion, onFailedGetItem: nil)
    }
    
    // MARK: - PokemonServiceDelegate
    
    func onSuccesfulGetPokemonList(_ pokemonData: PokemonData) {
        self.view?.dismissProgress()
        self.view?.updatePokemonData(pokemonData)
    }
    
    func onFailedGetPokemonList(_ error: HTTPFailedResponse) {
        self.view?.dismissProgress()
        self.view?.unknownError()
    }
    
    func onSuccesfulGetMoveList(_ moveData: MoveData) {
        self.view?.dismissProgress()
        self.view?.updateMoveData(moveData)
    }
    
    func onFailedGetMoveList(_ error: HTTPFailedResponse) {
        self.view?.dismissProgress()
        self.view?.unknownError()
    }
    
    func onSuccesfulGetItemList(_ itemData: ItemData) {
        self.view?.dismissProgress()
        self.view?.updateItemData(itemData)
    }
    
    func onFailedGetItemList(_ error: HTTPFailedResponse) {
        self.view?.dismissProgress()
        self.view?.unknownError()
    }
}
