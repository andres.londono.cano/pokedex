
import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var searchView: UIView!
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var dictationImageView: UIImageView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var pokemonButtonImageView: UIImageView!
    @IBOutlet private weak var movesButtonImageView: UIImageView!
    @IBOutlet private weak var itensButtonImageView: UIImageView!
    
    private let presenter = HomePresenter<HomeViewController>()
    
    private var originalData: [ModelWithName] = []
    private var data: [ModelWithName] = []
    private var selectedSection: HomeSectionType = .pokemon
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(self)
        self.pokemonButtonImageView.image = self.pokemonButtonImageView.image?.withRenderingMode(.alwaysTemplate)
        self.movesButtonImageView.image = self.movesButtonImageView.image?.withRenderingMode(.alwaysTemplate)
        self.itensButtonImageView.image = self.itensButtonImageView.image?.withRenderingMode(.alwaysTemplate)
        self.pokemonButtonImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectPokemonSection)))
        self.movesButtonImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectMovesSection)))
        self.itensButtonImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectItensSection)))
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "PokemonUITableViewCell", bundle: nil), forCellReuseIdentifier: "PokemonUITableViewCell")
        self.tableView.register(UINib(nibName: "MoveUITableViewCell", bundle: nil), forCellReuseIdentifier: "MoveUITableViewCell")
        self.tableView.register(UINib(nibName: "ItemUITableViewCell", bundle: nil), forCellReuseIdentifier: "ItemUITableViewCell")
        self.searchTextField.addTarget(self, action: #selector(searchTextDidChange), for: .editingChanged)
        self.setSection(section: selectedSection)
    }
    
    @objc private func selectPokemonSection() {
        self.setSection(section: .pokemon)
    }
    
    @objc private func selectMovesSection() {
        self.setSection(section: .moves)
    }
    
    @objc private func selectItensSection() {
        self.setSection(section: .itens)
    }
    
    private func setSection(section: HomeSectionType) {
        self.selectedSection = section
        self.searchTextField.text = nil
        switch section {
        case .pokemon:
            pokemonButtonImageView.tintColor = .black
            movesButtonImageView.tintColor = .lightGray
            itensButtonImageView.tintColor = .lightGray
            self.presenter.getPokemonList()
            break
        case .moves:
            pokemonButtonImageView.tintColor = .lightGray
            movesButtonImageView.tintColor = .black
            itensButtonImageView.tintColor = .lightGray
            self.presenter.getMoveList()
            break
        default:
            pokemonButtonImageView.tintColor = .lightGray
            movesButtonImageView.tintColor = .lightGray
            itensButtonImageView.tintColor = .black
            self.presenter.getItemList()
            break
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        switch selectedSection {
        case .pokemon:
            self.titleLabel.text = "Pokemon"
            if let pokemonItem = self.data[indexPath.row] as? PokemonResult, let url = pokemonItem.url, let pokemonCell = tableView.dequeueReusableCell(withIdentifier: "PokemonUITableViewCell", for: indexPath) as? PokemonUITableViewCell {
                self.presenter.getPokemon(url: url) { pokemon in
                    pokemonCell.pokemon = pokemon
                }
                cell = pokemonCell
            }
            break
        case .moves:
            self.titleLabel.text = "Moves"
            if let moveItem = self.data[indexPath.row] as? MoveResult, let url = moveItem.url, let moveCell = tableView.dequeueReusableCell(withIdentifier: "MoveUITableViewCell", for: indexPath) as? MoveUITableViewCell {
                self.presenter.getMove(url: url) { move in
                    moveCell.move = move
                }
                cell = moveCell
            }
            break
        case .itens:
            self.titleLabel.text = "Items"
            if let itemItem = self.data[indexPath.row] as? ItemResult, let url = itemItem.url, let itemCell = tableView.dequeueReusableCell(withIdentifier: "ItemUITableViewCell", for: indexPath) as? ItemUITableViewCell {
                self.presenter.getItem(url: url) { item in
                    itemCell.item = item
                }
                cell = itemCell
            }
            break
        }
        let backgroundView = UIView()
        backgroundView.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        cell.selectedBackgroundView = backgroundView
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension HomeViewController: HomeView {
    
    func updatePokemonData(_ pokemonData: PokemonData) {
        if selectedSection == .pokemon {
            self.originalData = pokemonData.results ?? []
            self.data = self.originalData
            self.tableView.reloadData()
        }
    }

    func updateMoveData(_ moveData: MoveData) {
        if selectedSection == .moves {
            self.originalData = moveData.results ?? []
            self.data = self.originalData
            self.tableView.reloadData()
        }
    }
    
    func updateItemData(_ itemData: ItemData) {
        if selectedSection == .itens {
            self.originalData = itemData.results ?? []
            self.data = self.originalData
            self.tableView.reloadData()
        }
    }
    
    func unknownError() {
        if let alertViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "SimpleAlertViewController") as? SimpleAlertViewController {
            alertViewController.modalPresentationStyle = .overCurrentContext
            alertViewController.modalTransitionStyle = .crossDissolve
            alertViewController.set(title: "Error", description: "An error has occurred, please try again", buttonTitle: "Understood")
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    func showProgress(_ message: String?) {
        activityIndicator.startAnimating()
    }
    
    func dismissProgress() {
        activityIndicator.stopAnimating()
    }
}

extension HomeViewController {
    
    @objc private func searchTextDidChange() {
        guard let searchText = searchTextField.text else {
            return
        }
        if (searchText.count > 0) {
            activityIndicator.startAnimating()
            self.data = self.data.filter {
                $0.name?.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
        } else {
            self.data = self.originalData
        }
        activityIndicator.stopAnimating()
        self.tableView.reloadData()
    }
}

enum HomeSectionType {
    case pokemon
    case moves
    case itens
}
