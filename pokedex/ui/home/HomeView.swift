
import Foundation

/// Home view declaration
protocol HomeView: BaseView {
    
    /// Updates pokemon information
    ///
    /// - Parameter pokemonData;  Pokemon response
    func updatePokemonData(_ pokemonData: PokemonData)
    
    /// Updates moves information
    ///
    /// - Parameter moveData;  Moves response
    func updateMoveData(_ moveData: MoveData)

    /// Updates ite,s information
    ///
    /// - Parameter itemData;  Items response
    func updateItemData(_ itemData: ItemData)
    
    /// Called when an unhandled error occurs
    func unknownError()
}
