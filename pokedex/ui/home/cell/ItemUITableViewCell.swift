
import UIKit

class ItemUITableViewCell: UITableViewCell {
    
    @IBOutlet weak var ballImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    
    var item: Item = Item() {
        didSet {
            if let sprite = item.sprites?.front_default, let url = URL(string: sprite) {
                ballImageView.load(url: url)
            } else {
                ballImageView.image = nil
            }
            nameLabel.text = item.name?.capitalizingFirstLetter()
            costLabel.text = "\(item.cost ?? 0)"
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        self.item = Item()
    }
}
