
import UIKit

class MoveUITableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeImageView: UIImageView!
    
    var move: Move = Move() {
        didSet {
            nameLabel.text = move.name?.capitalizingFirstLetter()
            typeImageView.image = getTypeImage(move.type?.name ?? "")
        }
    }
    
    private func getTypeImage(_ name: String) -> UIImage? {
        return UIImage(named: "ic_" + name)
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        self.move = Move()
    }
}
