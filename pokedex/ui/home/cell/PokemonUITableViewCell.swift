
import UIKit

class PokemonUITableViewCell: UITableViewCell {
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var typeStackView: UIStackView!
    
    var pokemon: Pokemon = Pokemon() {
        didSet {
            avatarImageView.image = nil
            if let sprite = pokemon.sprites?.other?.official_artwork?.front_default, let url = URL(string: sprite) {
                avatarImageView.load(url: url)
            }
            nameLabel.text = pokemon.name?.capitalizingFirstLetter()
            orderLabel.text = "#\(pokemon.order ?? 0)"
            typeStackView.removeAllArrangedSubviews()
            for imageView in getTypeImageViews(types: pokemon.types ?? []) {
                typeStackView.addArrangedSubview(imageView)
            }
        }
    }
    
    private func getTypeImageViews(types: [PokemonType]) -> [UIImageView] {
        var images: [UIImageView] = []
        for type in types {
            if let name = type.type?.name, let image = getTypeImage(name) {
                let imageView = UIImageView()
                imageView.contentMode = .scaleAspectFit
                imageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
                imageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
                imageView.image = image
                images.append(imageView)
            }
        }
        return images
    }
    
    private func getTypeImage(_ name: String) -> UIImage? {
        return UIImage(named: "ic_" + name)
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        self.pokemon = Pokemon()
    }
}
