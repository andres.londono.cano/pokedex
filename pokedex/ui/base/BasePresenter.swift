
import Foundation

/// Base presenter protocol declaration
protocol BasePresenterProtocol: NSObjectProtocol {
    
    associatedtype V
    
    /// The view to be presented
    var view: V? { get }
    
    /// Attaches the view to presenter
    /// - Parameter view: The view to be attached
    func attachView(_ view: V)
    
    /// Detaches the view from presenter
    func detachView()
}

/// Base presenter implementation
///
/// Generic parameter V must be a BaseView subclass
class BasePresenter<V: BaseView>: NSObject, BasePresenterProtocol {
    
    weak internal var view: V?
    
    func attachView(_ view: V) {
        self.view = view
    }
    
    func detachView() {
        self.view = nil
    }
}

