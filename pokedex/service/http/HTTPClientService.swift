
import Alamofire

/// Service that allows HTTP connections
class HTTPClientService {
    
    private let logger = BasicLogger(String(describing: HTTPClientService.self))
    
    /// Performs an HTTP request
    ///
    /// - Parameter url: Url of the endpoint
    /// - Parameter params: Key - value dictionary
    /// - Parameter headers: Key - value headers dictionary
    /// - Parameter httpMethod: HTTP method
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    private func request(url: String, params: [String: Any]? = nil, headers: [String : String]? = nil, httpMethod: HTTPMethod, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        self.logger.i("START ->", httpMethod.rawValue, url)
        self.logger.i("PARAMS:", params ?? "No params")
        AF.request(url, method: httpMethod, parameters: params, headers: self.getHTTPHeaders(headers: headers)).response { response in
            self.process(response: response, onSuccessful: onSuccessful, onFailed: onFailed)
        }
    }
    
    /// Performs an HTTP request
    ///
    /// - Parameter url: Url of the endpoint
    /// - Parameter data: Body data
    /// - Parameter headers: Key - value headers dictionary
    /// - Parameter httpMethod: HTTP method
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    private func request(url urlString: String, data: Data?, headers: [String : String]? = nil, httpMethod: HTTPMethod, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        self.logger.i("START ->", httpMethod.rawValue, urlString)
        if let data = data {
            self.logger.i("DATA:", String(decoding: data, as: UTF8.self))
        }
        guard let url = URL(string: urlString) else {
            self.logger.e("ERROR:", HTTPStatusCode.badRequest, HTTPClientService.malformedUrl)
            self.logger.i("END ->", urlString)
            onFailed(HTTPFailedResponse(statusCode: .badRequest, description: HTTPClientService.malformedUrl))
            return
        }
        AF.request(self.getURLRequest(url: url, data: data, headers: headers, method: httpMethod)).response { response in
            self.process(response: response, onSuccessful: onSuccessful, onFailed: onFailed)
        }
    }
    
    /// Converts a Key - value headers to HTTPHeaders object
    ///
    /// - Parameter headers: Key - value headers dictionary
    /// - Returns: Object of HTTPHeaders type
    private func getHTTPHeaders(headers: [String : String]?) -> HTTPHeaders? {
        var httpHeaders: HTTPHeaders?
        if let headers = headers {
            httpHeaders = HTTPHeaders()
            for (name, value) in headers {
                httpHeaders?.add(name: name, value: value)
            }
        }
        return httpHeaders
    }
    
    /// Retrieves an URLRequest
    ///
    /// - Parameter url: Request url
    /// - Parameter data: Body data
    /// - Parameter headers: Key - value headers dictionary
    /// - Parameter httpMethod: HTTP method
    /// - Returns: Object of URLRequest type
    private func getURLRequest(url: URL, data: Data?, headers: [String : String]? = nil, method: HTTPMethod) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpBody = data
        urlRequest.httpMethod = method.rawValue
        if let httpHeaders = self.getHTTPHeaders(headers: headers) {
            urlRequest.headers = httpHeaders
        }
        return urlRequest
    }
    
    /// Processes an AlamoFire response
    ///
    /// - Parameter response: Response to be processed
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    private func process(response: AFDataResponse<Data?>, onSuccessful: (_ response: HTTPSuccessfulResponse) -> Void, onFailed: (_ response: HTTPFailedResponse) -> Void) {
        switch response.result {
        case .failure(let error):
            self.logger.e("ERROR:", response.response?.statusCode ?? String.empty, error)
            self.logger.i("END ->", response.response?.url ?? String.empty)
            onFailed(HTTPFailedResponse(statusCode: HTTPStatusCode.from(response.response?.statusCode) ?? .internalServerError, description: error.localizedDescription))
        case .success(let data):
            self.logger.i("RESPONSE:", response.response?.statusCode ?? String.empty, data != nil ? String(decoding: data!, as: UTF8.self) : "No data")
            self.logger.i("END ->", response.response?.url ?? String.empty)
            if let statusCode = HTTPStatusCode.from(response.response?.statusCode), statusCode.responseType != .success {
                var description: String?
                if let data = data {
                    description = String(decoding: data, as: UTF8.self)
                }
                onFailed(HTTPFailedResponse(statusCode: statusCode, description: description))
                return
            }
            onSuccessful(HTTPSuccessfulResponse(statusCode: HTTPStatusCode.from(response.response?.statusCode) ?? .ok, data: data))
        }
    }
}

// MARK: - HTTPClientServiceProtocol

extension HTTPClientService: HTTPClientServiceProtocol {
    
    func get(url: String, params: [String: Any]? = nil, headers: [String : String]? = nil, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        request(url: url, params: params, headers: headers, httpMethod: .get, onSuccessful: onSuccessful, onFailed: onFailed)
    }
    
    func post(url: String, data: Data?, headers: [String : String]? = nil, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        request(url: url, data: data, headers: headers, httpMethod: .post, onSuccessful: onSuccessful, onFailed: onFailed)
    }
    
    func put(url: String, data: Data?, headers: [String : String]? = nil, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        request(url: url, data: data, headers: headers, httpMethod: .put, onSuccessful: onSuccessful, onFailed: onFailed)
    }
    
    func delete(url: String, params: [String: Any]? = nil, headers: [String : String]? = nil, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        request(url: url, params: params, headers: headers, httpMethod: .delete, onSuccessful: onSuccessful, onFailed: onFailed)
    }
}

// MARK: - Constant strings

extension HTTPClientService {
    static let serverError = "Server Could not be reached."
    static let malformedUrl = "Malformed url."
}
