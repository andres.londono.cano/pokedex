
import Foundation

/// Service that manages Pokemon API HTTP connections
class PokemonService {
    
    private let apiClientService = APIClientService(baseUrl: BuildConfig.baseUrl)
    
    func getPokemonList(delegate: PokemonServiceDelegate) {
        self.apiClientService.get(apiPath: .getPokemonList) { (response: PokemonData) in
            delegate.onSuccesfulGetPokemonList(response)
        } onFailed: { response in
            delegate.onFailedGetPokemonList(response)
        }
    }
    
    func getPokemon(url: String, onSuccesfulGetPokemon: ((_ pokemon: Pokemon) -> Void)?, onFailedGetPokemon: ((_ error: HTTPFailedResponse) -> Void)?) {
        HTTPClientService().get(url: APIUtil.normalizeBaseUrl(url), onSuccessful: { response in
            if let response = response.data?.decode(to: Pokemon.self) {
                onSuccesfulGetPokemon?(response)
            } else {
                var description: String?
                if let data = response.data {
                    description = String(decoding: data, as: UTF8.self)
                }
                onFailedGetPokemon?(HTTPFailedResponse(statusCode: HTTPStatusCode.internalServerError, description: description))
            }
        }, onFailed: { response in
            onFailedGetPokemon?(response)
        })
    }
    
    func getMoveList(delegate: PokemonServiceDelegate) {
        self.apiClientService.get(apiPath: .getMoveList) { (response: MoveData) in
            delegate.onSuccesfulGetMoveList(response)
        } onFailed: { response in
            delegate.onFailedGetMoveList(response)
        }
    }
    
    func getMove(url: String, onSuccesfulGetMove: ((_ move: Move) -> Void)?, onFailedGetMove: ((_ error: HTTPFailedResponse) -> Void)?) {
        HTTPClientService().get(url: APIUtil.normalizeBaseUrl(url), onSuccessful: { response in
            if let response = response.data?.decode(to: Move.self) {
                onSuccesfulGetMove?(response)
            } else {
                var description: String?
                if let data = response.data {
                    description = String(decoding: data, as: UTF8.self)
                }
                onFailedGetMove?(HTTPFailedResponse(statusCode: HTTPStatusCode.internalServerError, description: description))
            }
        }, onFailed: { response in
            onFailedGetMove?(response)
        })
    }
    
    func getItemList(delegate: PokemonServiceDelegate) {
        self.apiClientService.get(apiPath: .getItemList) { (response: ItemData) in
            delegate.onSuccesfulGetItemList(response)
        } onFailed: { response in
            delegate.onFailedGetItemList(response)
        }
    }
    
    func getItem(url: String, onSuccesfulGetItem: ((_ item: Item) -> Void)?, onFailedGetItem: ((_ error: HTTPFailedResponse) -> Void)?) {
        HTTPClientService().get(url: APIUtil.normalizeBaseUrl(url), onSuccessful: { response in
            if let response = response.data?.decode(to: Item.self) {
                onSuccesfulGetItem?(response)
            } else {
                var description: String?
                if let data = response.data {
                    description = String(decoding: data, as: UTF8.self)
                }
                onFailedGetItem?(HTTPFailedResponse(statusCode: HTTPStatusCode.internalServerError, description: description))
            }
        }, onFailed: { response in
            onFailedGetItem?(response)
        })
    }
}

protocol PokemonServiceDelegate {
    
    func onSuccesfulGetPokemonList(_ pokemonData: PokemonData)
    
    func onFailedGetPokemonList(_ error: HTTPFailedResponse)
    
    func onSuccesfulGetMoveList(_ moveData: MoveData)
    
    func onFailedGetMoveList(_ error: HTTPFailedResponse)
    
    func onSuccesfulGetItemList(_ moveData: ItemData)
    
    func onFailedGetItemList(_ error: HTTPFailedResponse)
}
