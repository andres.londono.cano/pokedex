
import Foundation

/// API utilities
class APIUtil {
    
    /// Removes `"/"` sufix to given base url
    public static func normalizeBaseUrl(_ baseUrl: String) -> String {
        var newBaseUrl = baseUrl
        if !newBaseUrl.hasSuffix("/") {
            newBaseUrl += "/"
        }
        return newBaseUrl
    }
    
    /// Removes `"/"` prefix to given path
    public static func normalizePath(_ path: String) -> String {
        if path.hasPrefix("/") {
            return self.normalizePath(String(path.dropFirst()))
        }
        return path
    }
}
