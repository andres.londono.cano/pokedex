
import Foundation

/// Relative endpoint paths
enum APIPath: String {
    
    case getPokemonList = "pokemon"
    case getMoveList = "move"
    case getItemList = "item"
}
