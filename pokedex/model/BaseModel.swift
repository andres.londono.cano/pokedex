
import Foundation

/// Base model declaration
///
/// Implements Codable protocol for serialization
protocol BaseModel: Codable {}
