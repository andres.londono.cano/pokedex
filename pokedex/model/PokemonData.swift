
import Foundation

/// Model for pokemon response
struct PokemonData: BaseModel {
    var count: Int?
    var previous: String?
    var next: String?
    var results: [PokemonResult]?
}

/// Model for pokemon response
struct PokemonResult: BaseModel, ModelWithName {
    var name: String?
    var url: String?
}

/// Model for pokemon response
struct Pokemon: BaseModel {
    var order: Int?
    var name: String?
    var sprites: Sprites?
    var types: [PokemonType]?
}

/// Model for pokemon sprites response
struct Sprites: BaseModel {
    var front_default: String?
    var other: OtherSprites?
}

/// Model for pokemon other sprites response
struct OtherSprites: BaseModel {
    var official_artwork: SpriteArtwork?
    
    enum CodingKeys: String, CodingKey {
        case official_artwork = "official-artwork"
    }
}

/// Model for pokemon Sprite art work response
struct SpriteArtwork: BaseModel {
    var front_default: String?
}

struct PokemonType: BaseModel {
    var type: PokemonTypeDetail?
}

struct PokemonTypeDetail: BaseModel {
    var name: String?
}


protocol ModelWithName {
    var name: String? { get set }
}
