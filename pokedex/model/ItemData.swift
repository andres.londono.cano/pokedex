
import Foundation

/// Model for item data response
struct ItemData: BaseModel {
    var count: Int?
    var previous: String?
    var next: String?
    var results: [ItemResult]?
}

/// Model for item result
struct ItemResult: BaseModel, ModelWithName {
    var name: String?
    var url: String?
}

/// Model for item response
struct Item: BaseModel {
    var name: String?
    var sprites: ItemSprites?
    var cost: Int?
}

/// Model for Item sprite response
struct ItemSprites: BaseModel {
    var front_default: String?
    
    enum CodingKeys: String, CodingKey {
        case front_default = "default"
    }
}
