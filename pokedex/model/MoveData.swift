
import Foundation

/// Model for move data response
struct MoveData: BaseModel {
    var count: Int?
    var previous: String?
    var next: String?
    var results: [MoveResult]?
}

/// Model for move result
struct MoveResult: BaseModel, ModelWithName {
    var name: String?
    var url: String?
}

/// Model for move response
struct Move: BaseModel {
    var name: String?
    var type: PokemonTypeDetail?
}
