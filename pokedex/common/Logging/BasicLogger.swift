
import Foundation

/// Simple logger class
class BasicLogger {
    
    private var tag: String?
    private let showingTrace: Bool
    
    init(showingTrace: Bool = false) {
        self.showingTrace = showingTrace
    }
    
    init(_ tag: String, showingTrace: Bool = false) {
        self.showingTrace = showingTrace
        self.tag = " - " + tag
    }
    
    /// Prints a verbose log
    ///
    /// - Parameter messages: Array of objects to be printed
    func v(_ messages: Any..., file: String = #file, function: String = #function, line: Int = #line) {
        print(self.getMessage(prefix: LogPrefix.verbose, messages: messages, file: file, function: function, line: line))
    }
    
    /// Prints a debug log
    ///
    /// - Parameter messages: Array of objects to be printed
    func d(_ messages: Any..., file: String = #file, function: String = #function, line: Int = #line) {
        print(self.getMessage(prefix: LogPrefix.debug, messages: messages, file: file, function: function, line: line))
    }
    
    /// Prints an info log
    ///
    /// - Parameter messages: Array of objects to be printed
    func i(_ messages: Any..., file: String = #file, function: String = #function, line: Int = #line) {
        print(self.getMessage(prefix: LogPrefix.info, messages: messages, file: file, function: function, line: line))
    }
    
    /// Prints a warning log
    ///
    /// - Parameter messages: Array of objects to be printed
    func w(_ messages: Any..., file: String = #file, function: String = #function, line: Int = #line) {
        print(self.getMessage(prefix: LogPrefix.warning, messages: messages, file: file, function: function, line: line))
    }
    
    /// Prints an error log
    ///
    /// - Parameter messages: Array of objects to be printed
    func e(_ messages: Any..., file: String = #file, function: String = #function, line: Int = #line) {
        print(self.getMessage(prefix: LogPrefix.error, messages: messages, file: file, function: function, line: line))
    }
    
    /// Retrieves the message to be printed
    ///
    /// - Parameter prefix: A string to be placed before message
    /// - Parameter messages: Array of objects to be printed
    /// - Parameter file: File where log was called from
    /// - Parameter function: Function where log was called from
    /// - Parameter line: Line where log was called from
    /// - Returns: The message to be printed
    private func getMessage(prefix: String, messages: [Any], file: String, function: String, line: Int) -> String {
        return self.getDate() + " " + prefix + (self.tag ?? "") + ": " + messages.toPrint + "\n\(self.getTrace(file: file, function: function, line: line))"
    }
    
    /// Retrieves the location where the log was called from
    ///
    /// - Parameter file: File where log was called from
    /// - Parameter function: Function where log was called from
    /// - Parameter line: Line where log was called from
    /// - Returns: The location where the log was called from
    private func getTrace(file: String, function: String, line: Int) -> String {
        if (self.showingTrace) {
            return "\tat \(file.split(separator: "/").last ?? "") -> \(function):\(line)\n"
        }
        return .empty
    }
    
    /// Retrieves the current date and time
    ///
    /// - Returns: Current date and time
    private func getDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "y-MM-dd H:m:ss.SSSS"
        return formatter.string(from: Date())
    }
    
    /// Prefixes for logging
    struct LogPrefix {
        static let verbose = "⚪ VERBOSE"
        static let debug = "🟢 DEBUG"
        static let info = "🔵 INFO"
        static let warning = "🟠 WARNING"
        static let error = "🔴 ERROR"
    }
}
